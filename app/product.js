import React from 'react';
import image from './../img/1.png'
import Rating from './rate';
export default class Product extends React.Component {
    constructor() {
        super();
        this.state = {
            brand: "Nikon",
            model: "D3000",
            price: "3999$",
            features: ['Good zoom', 'Long live time on battery', 'Good quality'],
            rating: 4.6,
            imgPath: '../../img/1.png'
        }

        this.state.featuresList = 
            this.state.features.map((feature) => {
            return (
                <li key={feature}>
                    <a>{feature}</a>
                </li>)
            });
    



    }
    render() {
        return (
            <div className="product">
                <a className="model">{this.state.brand} {this.state.model}</a> < br />
                <a className="price">Price: {this.state.price}</a> <br />
                <img className='productImg' src='./../img/1.png' />
                <ul>
                    {this.state.featuresList}
                </ul>
                <Rating rating={this.state.rating}/>
            </div>
        );
    }
}
