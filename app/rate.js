import React from 'react';
import style from './main.scss';
import starsPath from './../img/star.png'


export default class Rating extends React.Component {
    render() {
        return (
            <div className='rating'>
                <h3> Rating: {this.props.rating} / 5 (6 reviews) </h3>
                <img className="stars" style={this.countMask(this.props.rating)} src={starsPath} />
            </div>
        )

        
    }
    countMask(rating){
        var percentage = rating * 2 * 10;
        percentage = 100 - percentage;
        
        return {clipPath :`inset(0px ${percentage}% 0px 0%)`}
    }
}