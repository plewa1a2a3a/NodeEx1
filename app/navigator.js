import React from 'react';
import style from './main.scss';

export default class Navigator extends React.Component {
    render() {
        return (
            <div className="navigator">
                <select className="dispIB">
                    <option value="smartphones">Smartphones</option>
                    <option value="tablets">Tablets</option>
                    <option value="laptops">Laptops</option>
                </select>
                <form className="dispIB" onSubmit={this.mySubmitEvent()}>
                    <input type="text" placeholder="What do I need to do?" ref="createInput" />
                    <button>Create</button>
                </form>
            </div>
        )
    }
    mySubmitEvent(){
        console.log("submit done")
    }
}
