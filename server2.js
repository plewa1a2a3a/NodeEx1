"use strict";

  const fs = require('fs');
var express = require('express')
var app = express()
var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'trace'
});
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// app.get('/', function (req, res) {
//     let result = elasticRequest(res)
// })

app.listen(3001, function () {
    console.log('Example app listening on port 3001!')
})


// function elasticRequest(res) {
//     client.search({
//         index: 'shop',
//         type: 'cameras',
//         body: {
//             query: {
//                 match_all: {}
//             },
//         }
//     }, function (error, response, status) {
//         var myHits = [];
//         if (error) {
//             console.log("search error: " + error)
//         }
//         else {
//             response.hits.hits.forEach(function (hit) {
//                 console.log(hit);
//                 myHits.push(hit);
//             })
//             res.json(myHits);
//         }
//     });

// }


// const bulkIndex = function bulkIndex(index, type, data) {
//     let bulkBody = [];

//     data.forEach(item => {
//       bulkBody.push({
//         index: {
//           _index: index,
//           _type: type,
//           _id: item.id
//         }
//       });

//       bulkBody.push(item);
//     });

//     client.bulk({body: bulkBody})
//     .then(response => {
//       let errorCount = 0;
//       response.items.forEach(item => {
//         if (item.index && item.index.error) {
//           console.log(++errorCount, item.index.error);
//         }
//       });
//       console.log(`Successfully indexed ${data.length - errorCount} out of ${data.length} items`);
//     })
//     .catch(console.err);
//   };

//   // only for testing purposes
//   // all calls should be initiated through the module
//   const test = function test() {
//     const camerasRaw = fs.readFileSync('products.json');
//     const cameras = JSON.parse(camerasRaw);
//     console.log(`${cameras.length} items parsed from data file`);
//     bulkIndex('shop', 'cameras', cameras);
//   };

//   //test();